#+TITLE:      Toon's pry configuration
#+AUTHOR:     Toon Claes
#+KEYWORDS:   toon claes pry ruby config
#+STARTUP:    showall
#+PROPERTY:   header-args+ :comments both
#+PROPERTY:   header-args+ :tangle "~/.pryrc"
#+PROPERTY:   header-args+ :shebang "#-*- mode: ruby -*-"
#+PROPERTY:   header-args+ :tangle-mode (identity #o644)
-----

* Pry

This file configures pry, the enhanced REPL for ruby.

** Debugging

*** Stepping code

Enable short commands for stepping around while debugging.

#+BEGIN_SRC ruby
if defined?(PryByebug)
  Pry.commands.alias_command 'c', 'continue'
  Pry.commands.alias_command 's', 'step'
  Pry.commands.alias_command 'n', 'next'
  Pry.commands.alias_command 'f', 'finish'
end
#+END_SRC

*** Repeat last command

Press ~ENTER~ will repeat the last command over again. Very convenient
to step several lines of code.

#+BEGIN_SRC ruby
# Hit Enter to repeat last command
Pry::Commands.command /^$/, "repeat last command" do
  _pry_.run_command Pry.history.to_a.last
end
#+END_SRC
